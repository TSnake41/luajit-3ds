/*
  Copyright (C) 2019 Teddy ASTIE

  Permission to use, copy, modify, and/or distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/

#ifndef __SLDL_H__
#define __SLDL_H__

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

/* A symbol (with name and it's corresponding pointer). */
struct sldl_symbol {
  const char *name;
  void *addr;
};

struct sldl_module {
  const char *name;
  struct sldl_symbol *sym;
  bool global;
};

typedef struct sldl_module sldl_env[];

/* Set a environment (table contining all stuff).
 * Environment is expected to be persistent (e.g global variable).
 */
void sldl_setenv(sldl_env *env);

int sldl_dlclose(void *handle);
char *sldl_dlerror(void);
void *sldl_dlopen(const char *name);
void *sldl_dlsym(void *restrict handle, const char *restrict name);

#ifdef SLDL_IMPLEMENTAION

static sldl_env *sldl_global_env = NULL;

static const char *const sldl_msg_success = "No error";
static const char *sldl_error = sldl_msg_success;

void sldl_setenv(sldl_env *env)
{
  sldl_global_env = env;
}

void *sldl_dlopen(const char *name)
{
  if (sldl_global_env == NULL)
    return NULL;

  if (name == NULL)
    return NULL;

  struct sldl_module *current = *sldl_global_env;
  while (current->name) {
    if (strcmp(current->name, name) == 0) {
      sldl_error = sldl_msg_success;
      return current;
    }

    current++;
  }

  sldl_error = "Module not found";
  return NULL;
}

int sldl_dlclose(void *handle)
{
  /* Do nothing (handle is just a reference). */
  (void)handle;
  return 0;
}

char *sldl_dlerror(void)
{
  return (char *)sldl_error;
}

static void *sldl_search_sym(struct sldl_module *mod, const char *name)
{
  struct sldl_symbol *current = mod->sym;

  while (current->name) {
    if (strcmp(current->name, name) == 0)
      /* Symbol found ! */
      return current->addr;

    current++;
  }

  return NULL;
}

void *sldl_dlsym(void *restrict handle, const char *restrict name)
{
  if (sldl_global_env == NULL)
    return NULL;

  const char *const not_found = "Symbol not found.";

  /* Clear error. */
  sldl_error = sldl_msg_success;

  if (handle == NULL) {
    /* Search though all global modules. */
    struct sldl_module *current = *sldl_global_env;

    while (current->name) {
      if (current->global) {
        /* Explore inside it */
        void *sym = sldl_search_sym(current, name);

        if (sym) /* If we found it, take it */
          return sym;
      }

      current++;
    }
  } else {
    /* Search though module pointer by handle. */
    struct sldl_module *mod = handle;

    void *sym = sldl_search_sym(mod, name);

    if (sym)
      return sym;
  }

  sldl_error = not_found;
  return NULL;
}

#endif /* SLDL_IMPLEMENTAION */

#endif /* __SLDL_H__ */
